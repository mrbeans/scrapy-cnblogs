# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
from scrapy.exceptions import DropItem

class BlogsPipeline(object):
    def __init__(self):
        self.file=open('shanyou.json','w+',encoding='utf-8')
    def process_item(self, item, spider):
        if(item['title']):
            #line=json.dumps(dict(item))+'\n'
            #self.file.write(line.encode('utf-8').decode('unicode_escape'))
            l=item['title']#.encode('utf-8').decode('unicode_escape')
            print(l)
            self.file.write(l)
            return item
        else:
            raise DropItem('missing title in %s'%item)
