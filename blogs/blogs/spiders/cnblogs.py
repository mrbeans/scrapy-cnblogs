# -*- coding: utf-8 -*-
import scrapy
from blogs.items import BlogsItem
from scrapy.selector import Selector

class CnblogsSpider(scrapy.Spider):
    name = 'cnblogs'
    allowed_domains = ['cnblogs.com']
    start_urls = ['https://www.cnblogs.com/shanyou/']

    def parse(self, response):
        blogs=response.xpath(".//*[@class='postTitle']")
        for blog in blogs:
            title=blog.xpath("./a/text()").extract()[0]
            href=blog.xpath("./a/@href").extract()[0]
            #fileName=self.start_urls[0].lstrip('https://www.cnblogs.com/').rstrip('/')
            item=BlogsItem(title=title,href=href)
            yield item
        next_page=Selector(response).re(u'<a href="(\S*)">下一页</a>')
        if(next_page):
            yield scrapy.Request(url=next_page[0],callback=self.parse)
